package game.server.manager.server.qo;

import game.server.manager.mybatis.plus.qo.MpBaseQo;
import game.server.manager.server.entity.DockerDetails;

import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * docker配置信息查询对象
 * 
 * @author yuzhanfeng
 * @date 2022-11-13 12:10:30
 */
@Data
@NoArgsConstructor
public class DockerDetailsQo extends MpBaseQo<DockerDetails> {

}
